#ifndef Arduino_h
#define Arduino_h

#ifndef ARDUINO
#error "Please add -DARDUINO=100 to your compiler flags"
#endif

#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <stdint.h>

typedef uint8_t byte;
typedef bool boolean;

#ifdef __cplusplus
#include <WString.h>
#include <Print.h>
#include <Stream.h>
#include <SoftwareSerial.h>
#endif

#ifdef __cplusplus
extern "C"{
#endif

void yield(void);
void delay(unsigned long ms);
unsigned long micros();
unsigned long millis();

#define HIGH 0x1
#define LOW  0x0

#define INPUT 0x0
#define OUTPUT 0x1
#define INPUT_PULLUP 0x2

void pinMode(uint8_t pin, uint8_t mode);
void digitalWrite(uint8_t pin, uint8_t val);
int digitalRead(uint8_t pin);

#ifdef __cplusplus
};
#endif

#endif

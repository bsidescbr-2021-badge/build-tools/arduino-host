#include <Arduino.h>

static uint8_t g_pins = 0;

void pinMode(uint8_t pin, uint8_t mode)
{
}

void digitalWrite(uint8_t pin, uint8_t val)
{
    if ( pin < 8 )
    {
        if ( val )
        {
            g_pins |= 1 << pin;
        }
        else
        {
            g_pins &=~ (1 << pin);
        }
    }
}

int digitalRead(uint8_t pin)
{
    if ( pin < 8 )
    {
        if ( ( g_pins & (1 << pin) ) != 0)
        {
            return HIGH;
        }
    }
    return LOW;
}

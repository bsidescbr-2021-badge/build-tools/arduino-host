#include <unistd.h>
#include <time.h>

#include "Arduino.h"

static bool g_init = false;
static struct timespec  g_t_zero;

int clock_gettime_progstart(clockid_t clk_id, struct timespec *tp)
{
    int err;

    if (!g_init)
    {
        memset(&g_t_zero, 0, sizeof(g_t_zero));
        err = clock_gettime(CLOCK_MONOTONIC, &g_t_zero);
        if (err)
            return err;
        g_init = true;
    }
    err = clock_gettime(CLOCK_MONOTONIC, tp);
    if (err)
        return err;
    tp->tv_sec -= g_t_zero.tv_sec;
    return 0;
}

unsigned long micros() {
    unsigned long       us;
    struct timespec     t;

    memset(&t, 0, sizeof(t));
    clock_gettime_progstart(CLOCK_MONOTONIC, &t);

    us = 0;
    us += ((unsigned long)t.tv_sec * 1000 * 1000);
    us += ((unsigned long)t.tv_nsec / 1000);

    return us;
}

unsigned long millis()
{
    unsigned long       ms;
    struct timespec     t;

    memset(&t, 0, sizeof(t));
    clock_gettime_progstart(CLOCK_MONOTONIC, &t);

    ms = 0;
    ms += ((unsigned long)t.tv_sec * 1000);
    ms += ((unsigned long)t.tv_nsec / (1000 * 1000));

    return ms;
}

void delay(unsigned long ms)
{
    struct timespec r = { .tv_sec = ms / 1000, .tv_nsec = (ms % 1000) * 1e6 };
    while (nanosleep(&r, &r) == -1);
}

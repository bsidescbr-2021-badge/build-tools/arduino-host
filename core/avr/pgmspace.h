#ifndef __PGMSPACE_H_
#define __PGMSPACE_H_

#define PROGMEM

#define PGM_P           char *

#define pgm_read_byte(address_short)    (*((unsigned char*)(address_short)))

#define strcpy_P strcpy

#define strlen_P strlen

#endif

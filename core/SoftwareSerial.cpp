#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <inttypes.h>
#include <unistd.h>
#include "Arduino.h"

#include "SoftwareSerial.h"

// SerialEvent functions are weak, so when the user doesn't define them,
// the linker just sets their address to 0 (which is checked below).
// The Serialx_available is just a wrapper around Serialx.available(),
// but we can refer to it weakly so we don't pull in the entire
// SoftwareSerial instance if the user doesn't also refer to it.
#if defined(HAVE_HWSERIAL0)
    void serialEvent() __attribute__((weak));
    bool Serial0_available() __attribute__((weak));
#endif

void serialEventRun(void)
{
#if defined(HAVE_HWSERIAL0)
    if (Serial0_available && serialEvent && Serial0_available()) serialEvent();
#endif
}

void SoftwareSerial::begin(unsigned long baud, byte config)
{
    // nothing to do
}

void SoftwareSerial::end()
{
    // nothing to do
}

int SoftwareSerial::available(void)
{
    return true;
}

int SoftwareSerial::peek(void)
{
    return -1;
}

int SoftwareSerial::read(void)
{
    return -1;
}

int SoftwareSerial::availableForWrite(void)
{
    return true;
}

void SoftwareSerial::flush()
{
    fflush(NULL);
}

size_t SoftwareSerial::write(uint8_t c)
{
    putchar((int)c);
    fflush(NULL);
    return 1;
}

SoftwareSerial Serial;
